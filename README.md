
## Instalación

Lo que necesitamos para correr este pequeño sistema es un servidor apache con php, para esto instalamos xampp y activamos el servidor apache, para la base de datos usamos postgresql, para que php pueda hacer las consultas a postgres debemos activar la extension pgsql en el archivo php.ini

```bash
;extension=pdo_firebird
extension=pdo_mysql
;extension=pdo_oci
;extension=pdo_odbc
extension=pdo_pgsql <-- quitamos el punto y coma al inicio
extension=pdo_sqlite
extension=pgsql <-- quitamos el punto y coma al inicio
;extension=shmop
```
    
### Base de datos

Para migrar la base de datos, debemos usar pgAdmin y crear una nueva base de datos llamada votacion e importar el archivo votacion.sql (en la carpeta sql) o copiar el texto dentro del archivo y ejecutarlo con el query tool

### Versiones

PHP 8.1.10
PostgreSQL v15