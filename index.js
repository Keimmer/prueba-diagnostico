function validRUT(rut) {
  const regexrut = /\b[0-9|.]{1,10}\-[K|k|0-9]/g;
  return regexrut.test(rut);
}

function validEmail(email) {
  const regexemail =
    /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g;
  return regexemail.test(email);
}

function validAlias(alias) {
  const regexAlias = /[a-zA-Z0-9]{5,}/g;
  return regexAlias.test(alias);
}

// buscamos los elementos checkbox
const checks = document.querySelectorAll("#ch");

// creamos un arreglo para los check seleccionados
let medios = [];
// agregamos un event listener para cada uno
checks.forEach((check) => {
  check.addEventListener("change", function () {
    if (this.checked) {
      if (medios.filter((med) => med == this.name) > 0) {
      } else {
        medios.push(this.name);
      }
    } else {
      medios = medios.filter((med) => med != this.name);
    }
    console.log(medios);
  });
});

// buscamos el elemento form en el archivo html
const element = document.querySelector("form");
// agregamos un event listener
element.addEventListener("submit", (event) => {
  event.preventDefault();
  // seleccionamos todos los campos del formulario
  const nameAndLastName = document.getElementById("nameLastName").value;
  const alias = document.getElementById("alias").value;
  const rut = document.getElementById("rut").value;
  const email = document.getElementById("email").value;
  const comunaSeleccionada = document.getElementById("comunas").value;
  const candidatoSeleccionado = document.getElementById("candidatos").value;

  // validamos el fromulario
  if (
    validAlias(alias) &&
    validRUT(rut) &&
    validEmail(email) &&
    nameAndLastName.length > 0 &&
    comunaSeleccionada > 0 &&
    candidatoSeleccionado.length > 0 &&
    medios.length > 1
  ) {
    console.log("Se envia la informacion a php");
    var data = new FormData();
    data.append("name", nameAndLastName);
    data.append("alias", alias);
    data.append("rut", rut);
    data.append("email", email);
    data.append("comunaSeleccionada", comunaSeleccionada);
    data.append("candidatoSeleccionado", candidatoSeleccionado);
    data.append("medios", medios);

    const xmlhttp = new XMLHttpRequest();

    xmlhttp.onload = function () {
      const response = this.responseText;
      alert(response);
    };

    xmlhttp.open("POST", "php/index.php");
    xmlhttp.send(data);
  } else {
    let errors = [];
    if (validAlias(alias) == false) {
      errors.push(
        " Su alias debe contener solo letras y numeros y debe tener un minimo de 5 caracteres "
      );
    }
    if (validRUT(rut) == false) {
      errors.push(" RUT Invalido ");
    }
    if (validEmail(email) == false) {
      errors.push(" Email en formato invalido ");
    }
    if (nameAndLastName.length < 1) {
      errors.push(" Debe ingresar un nombre y apellido ");
    }
    if (comunaSeleccionada < 1) {
      errors.push(" Debe seleccionar la comuna a donde pertenece ");
    }
    if (candidatoSeleccionado.length < 1) {
      errors.push(" Debe seleccionar un candidato de la lista ");
    }
    if (medios.length < 2) {
      errors.push(" Debe seleccionar al menos 2 opciones en como se entero de nosotros ")
    }
    alert(errors);
  }
});

function getComunas() {
  // obtenemos la opcion seleccionada en el select de regiones
  var regSelect = document.getElementById("regiones");
  //var sleTex = regSelect.options[regSelect.selectedIndex].innerHTML;
  var selectedComuna = regSelect.value;

  // select de las comunas
  var comSelect = document.getElementById("comunas");

  // se vacian las opciones del select
  var options = document.querySelectorAll("#comunas option");
  options.forEach((o) => o.remove());

  const xmlhttp = new XMLHttpRequest();

  xmlhttp.onload = function () {
    const comunas = JSON.parse(this.responseText);
    console.log(comunas);
    //Ciclo de las comunas

    for (var i = 0; i < comunas.length; i++) {
      //Se crea una opcion por cada comuna y se agrega al select
      let opt = document.createElement("option");
      opt.value = comunas[i].id;
      opt.text = comunas[i].name;
      comSelect.add(opt);
    }
  };

  xmlhttp.open(
    "GET",
    "php/index.php?" +
      new URLSearchParams({
        get: "comunas",
        region_id: selectedComuna,
      })
  );
  xmlhttp.send();
}

function getRegiones() {
  // se busca el input select que contiene todas las opciones de regiones
  var select = document.getElementById("regiones");

  // se hace una nueva solicitud xml
  const xmlhttp = new XMLHttpRequest();
  xmlhttp.onload = function () {
    const regiones = JSON.parse(this.responseText);

    for (var i = 0; i < regiones.length; i++) {
      let opt = document.createElement("option");
      opt.value = regiones[i].id;
      opt.text = regiones[i].name;
      select.add(opt);
    }
  };

  xmlhttp.open(
    "GET",
    "php/index.php?" +
      new URLSearchParams({
        get: "regiones",
      })
  );
  xmlhttp.send();
}

function getCandidatos() {
  // se busca el input select que contiene todas las opciones de candidatos
  var select = document.getElementById("candidatos");

  // se hace una nueva solicitud xml
  const xmlhttp = new XMLHttpRequest();
  xmlhttp.onload = function () {
    const candidatos = JSON.parse(this.responseText);

    for (var i = 0; i < candidatos.length; i++) {
      let opt = document.createElement("option");
      opt.value = candidatos[i].rut;
      opt.text = candidatos[i].name;
      select.add(opt);
    }
  };

  xmlhttp.open(
    "GET",
    "php/index.php?" +
      new URLSearchParams({
        get: "candidatos",
      })
  );
  xmlhttp.send();
}

// al ejecutar el script se llama a la funcion get regiones y getcandidatos para que este listo el formulario
getRegiones();
getCandidatos();
