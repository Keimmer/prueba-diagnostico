<?php
// string de conexion a base de datos postgres
$connStr = "host=localhost port=5432 dbname=votacion user=postgres password=admin";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $rut = $_POST["rut"];
    $nameLastName = $_POST["name"];
    $alias = $_POST["alias"];
    $email = $_POST["email"];
    $comunaSeleccionada = $_POST["comunaSeleccionada"];
    $candidatoSeleccionado = $_POST["candidatoSeleccionado"];
    $medios = explode(",", $_POST["medios"]);

    // variable de conexion
    $conn = pg_connect($connStr);

    // debemos buscar si el rut ingresado ya esta registrado
    $result = pg_query($conn, "select * from votantes where rut like '{$rut}'");
    if (pg_num_rows($result) > 0) {
        // el votante ya esta registrado, se verifica si este ya voto por un candidato
        $resultVotos = pg_query($conn, "select * from votosPorCandidato where votante_id like '{$rut}'");
        if (pg_num_rows($resultVotos) > 0) {
            echo "ya voto";
        } else {
            pg_query($conn, "INSERT INTO votosPorCandidato (votante_id, candidato_id) VALUES ('{$rut}', '{$candidatoSeleccionado}')");
            echo "voto registrado";
        }
    } else {
        // si el votante no esta registrado se procede a registrarlo y se guarda su voto
        try {
            // registrando al votante
            pg_query($conn, "INSERT INTO votantes (rut, name, alias, email, comuna_id) VALUES ('{$rut}', '{$nameLastName}', '{$alias}', '{$email}', {$comunaSeleccionada})");
            // registrando su voto
            pg_query($conn, "INSERT INTO votosPorCandidato (votante_id, candidato_id) VALUES ('{$rut}', '{$candidatoSeleccionado}')");
            // registrando como se entero
            for ($i = 0; $i < count($medios); $i++) {
                $medioResult = pg_query($conn, "SELECT * FROM medioPublicitario WHERE medio LIKE '{$medios[$i]}'");
                $medioSelect = pg_fetch_all($medioResult);
                $cantidad = $medioSelect[0]['cantidad'] + 1;
                pg_query($conn, "UPDATE medioPublicitario SET cantidad = {$cantidad} WHERE medio LIKE '{$medios[$i]}'");
            }
            echo "voto registrado";
        } catch (\Throwable $th) {
            echo "ocurrio algun error";
        }
    }
} else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $request_params = $_GET["get"];
    switch ($request_params) {
        case 'regiones':
            // se hace la conexion
            $conn = pg_connect($connStr);
            // se ejecuta el query
            $result = pg_query($conn, "select * from regiones");
            // se ordenan los datos
            $data = pg_fetch_all($result);

            // se aplica un encabezado de contenido json
            header('Content-type: application/json');
            // se envia la repuesta en formato json
            echo json_encode($data);

            break;
        case 'comunas':
            $region_id = $_GET["region_id"];
            // se hace la conexion
            $conn = pg_connect($connStr);
            // se ejecuta el query
            $result = pg_query($conn, "select * from comunas where region_id = '{$region_id}'");
            // se ordenan los datos
            $data = pg_fetch_all($result);

            // se aplica un encabezado de contenido json
            header('Content-type: application/json');
            // se envia la repuesta en formato json
            echo json_encode($data);

            break;
        case 'candidatos':
            // se hace la conexion
            $conn = pg_connect($connStr);
            // se ejecuta el query
            $result = pg_query($conn, "select * from candidatos");
            // se ordenan los datos
            $data = pg_fetch_all($result);

            // se aplica un encabezado de contenido json
            header('Content-type: application/json');
            // se envia la repuesta en formato json
            echo json_encode($data);

            break;
        default:
            // Metodo de solicitud invalido
            header("HTTP/1.0 405 Method Not Allowed");
            break;
    }
}
